export namespace main {
	
	export class Message {
	    sender: string;
	    source: string;
	    time: string;
	    text: string;
	    key: string;
	
	    static createFrom(source: any = {}) {
	        return new Message(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.sender = source["sender"];
	        this.source = source["source"];
	        this.time = source["time"];
	        this.text = source["text"];
	        this.key = source["key"];
	    }
	}

}

