import 'bulma/css/bulma.min.css';
import {Section, Menu} from 'react-bulma-components';

type UserListProps = {
    users: string[]
}

function UserList({users} : UserListProps) {

    function renderListItem(nick: string) {
        return (
            <Menu.List.Item key={nick}>{nick}</Menu.List.Item>
        )
    }

    const items = users.map(function(nick: string) {
        return renderListItem(nick)
    })

    return (
        <Section className="p-3">
            <Menu>
                <Menu.List title="Online">
                    {items}
                </Menu.List>
            </Menu>
        </Section>
    )
}

export default UserList;