import { useEffect, useRef } from "react";
import { Content, Media, Icon } from "react-bulma-components";
import { main } from '../wailsjs/go/models';

type MessageListProps = {
    messages: main.Message[]
}

function MessageList(props: MessageListProps) {

    const messageEndRef = useRef<HTMLDivElement>(null)

    function scrollToBottom() {
        messageEndRef.current?.scrollIntoView({ behavior: "smooth" })
    }

    useEffect(scrollToBottom, [props])

    const iconStyle = {
        backgroundColor: "hsl(271, 100%, 71%)",
        color: "white",
        borderRadius: "2px",
        width: "40px",
        height: "40px",
        fontSize: "1.11em",
        fontWeight: "bold"
    }

    const articleStyle = {
        borderTop: "0"
    }

    const followUpArticleStyle = {
        borderTop: "0",
        marginTop: "0"
    }

    const followUpTimeStyle = {
        display: "block",
        width: "40px"
    }

    function renderMessage(message: main.Message, previousSender: string) {
        return message.sender != previousSender ? (
            <Media renderAs="article" style={articleStyle} key={message.key}>
                <Media.Item align="left">
                    <Icon style={iconStyle}>{message.sender.at(0)?.toUpperCase()}</Icon>
                </Media.Item>
                <Media.Item>
                    <Content>
                        <p>
                            <strong>{message.sender}</strong> <small>{message.time}</small>
                            <br />
                            {message.text}
                        </p>
                    </Content>
                </Media.Item>
            </Media>
        ) : (
            <Media renderAs="article" style={followUpArticleStyle} key={message.key}>
                <Media.Item align="left">
                    <small style={followUpTimeStyle}>
                        {message.time != previousTime ? message.time : ' '}
                    </small>
                </Media.Item>
                <Media.Item>
                    <Content>
                        <p>{message.text}</p>
                    </Content>
                </Media.Item>
            </Media>
        )
    }

    let previousSender = ''
    let previousTime = ''
    const items = props.messages?.map(function(message: main.Message) {
        const renderedMessage = renderMessage(message, previousSender)
        previousSender = message.sender
        previousTime = message.time
        return renderedMessage;
    })

    return (<div>
        {items}
        <div ref={messageEndRef}></div>
        </div>)
}

export default MessageList;