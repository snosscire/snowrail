import { useEffect, useState } from 'react';
import 'bulma/css/bulma.min.css';
import { Columns } from 'react-bulma-components';
import ServerList from './ServerList';
import Server from './Server';
import { EventsOff, EventsOn } from '../wailsjs/runtime/runtime'
import { GetServerNames } from "../wailsjs/go/main/App";

function ChatApp() {

    const [servers, setServers] = useState(new Array<string>) 
    const [selectedServer, setSelectedServer] = useState('')

    function handleServerConnecting(name: string) {
        const list = new Array<string>;
        list.push(name);
        setServers(servers.concat(list))
        setSelectedServer(name)
    }

    function handleServerConnected(name: string) {}

    function handleServerChange(name: string) {
        setSelectedServer(name)
    }

    function updateServers(servers: string[]) {
        if (servers.length > 0) {
            setServers(servers)
            setSelectedServer(servers[0])
        }
    }

    useEffect(function() {
        EventsOn('server.connecting', handleServerConnecting)
        EventsOn('server.connected', handleServerConnected)
    
        GetServerNames().then(updateServers)

        return function() {
            EventsOff('server.connecting')
            EventsOff('server.connected')
            EventsOff('channel.joined')
        }
    }, [])


    function renderServer(serverName: string) {
        return (
            <Server
                key={serverName}
                serverName={serverName} />
        )
    }

    const renderedServers = servers.map((serverName: string) => {
        return renderServer(serverName)
    })

    return (
        <Columns multiline={false} style={{minHeight: '100vh'}}>
            <Columns.Column narrow={true} backgroundColor="white-ter">
                <ServerList
                    servers={servers}
                    selectedServer={selectedServer}
                    onChange={handleServerChange} />
            </Columns.Column>
            <Columns.Column>
                {renderedServers}
            </Columns.Column>
        </Columns>
    )
}

export default ChatApp;