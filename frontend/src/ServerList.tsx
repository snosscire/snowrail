import 'bulma/css/bulma.min.css';
import { Button } from 'react-bulma-components';

type ServerListProps = {
    servers: string[]
    selectedServer: string
    onChange: (selected: string) => void
}

function ServerList({servers, selectedServer, onChange}: ServerListProps) {

    function selectServer(name: string) {
        if (selectedServer != name) {
            onChange(name)
        }
    }

    function renderListItem(name: string, selected: boolean) {
        const style = {width: "54px"};
        const color = selected ? 'primary' : 'white';
        return (
            <div key={name} className="mt-3 ml-3">
                <Button color={color} size="medium" style={style} onClick={(e: any) => selectServer(name)}>
                    {name.at(0)}
                </Button>
            </div>
        );
    }
    
    const serverList = servers.map(function(server: string) {
        const selected = selectedServer == server ? true : false;
        return renderListItem(server, selected);
    })

    return (
        <div>
            {serverList}
        </div>
    )
}

export default ServerList;