import MessageList from './MessageList';
import MessageInput from './MessageInput';
import { main } from '../wailsjs/go/models';

type MessagesProps = {
    channelName: string
    messages: main.Message[]
    onSend: (message: string) => void
}

function Messages(props: MessagesProps) {
    const style = {
        height: 'calc(100vh - 76px)',
        overflow: 'auto'
    }
    return (
        <div>
            <div style={style} className="chat pt-3 pb-3">
                <MessageList messages={props.messages} />
            </div>
            <div className="pt-3 pb-3">
                <MessageInput channelName={props.channelName} onSend={props.onSend} />
            </div>
        </div>
    )
}

export default Messages;
