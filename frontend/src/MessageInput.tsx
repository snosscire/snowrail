import { ChangeEvent, KeyboardEvent, useState } from 'react';
import { Form } from "react-bulma-components"

type MessageInputProps = {
    channelName: string
    onSend: (message: string) => void
}

function MessageInput(props: MessageInputProps) {
    const [message, setMessage] = useState('')

    function handleKeyDown(event: KeyboardEvent<HTMLInputElement>) {
        if (event.key == "Enter") {
            props.onSend(message)
            setMessage('')
        }
    }

    function handleChange(event: ChangeEvent<HTMLInputElement>) {
        setMessage(event.target.value)
    }

    const placeholder = "Message " + props.channelName;

    return (
        <Form.Input type="text"
            placeholder={placeholder}
            value={message}
            onChange={handleChange}
            onKeyDown={handleKeyDown}
        /> 
    )
}

export default MessageInput;