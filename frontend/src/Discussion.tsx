import 'bulma/css/bulma.min.css';
import { Columns } from 'react-bulma-components';
import Messages from './Messages';
import UserList from './UserList';
import { main } from '../wailsjs/go/models';

type DiscussionProps = {
    serverName: string
    channelName: string
    messages: main.Message[]
    users?: string[]
    active: boolean
    onMessageSend: (message: string) => void
}

function Discussion( props: DiscussionProps) {

    let userList;
    if (props.users) {
        userList = (
            <Columns.Column size={2} backgroundColor="white-bis">
                <UserList users={props.users} />
            </Columns.Column>
        )
    }

    if (props.active) {
        return (
            <Columns multiline={false} style={{minHeight: '100vh'}}>
                <Columns.Column>
                <Messages
                        channelName={props.channelName}
                        messages={props.messages}
                        onSend={props.onMessageSend} />
                </Columns.Column>
                {userList}
            </Columns>
        )
    }
    return (<></>);
}

export default Discussion