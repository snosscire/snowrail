import 'bulma/css/bulma.min.css';
import { Section, Menu, Tag } from 'react-bulma-components';

type DiscussionListProps = {
    title: string
    channels: string[]
    selectedChannel: string
    onChange: (selected: string) => void
}

function DiscussionList(props: DiscussionListProps) {
    function selectChannel(name: string) {
        if (props.selectedChannel != name) {
            props.onChange(name)
        }
    }

    function renderListItem(name: string, selected: boolean, unread: number) {
        const tag = (unread > 0
            ? <Tag color="info" style={{float: 'right'}}>{unread}</Tag>
            : '')
        return (
            <Menu.List.Item key={name} active={selected} onClick={(e: any) => selectChannel(name)}>
                {name} {tag}
            </Menu.List.Item>
        )
    }

    const items = props.channels.map(function(name: string) {
        const selected = props.selectedChannel == name ? true : false
        return renderListItem(name, selected, 0)
    })

    return (
        <Section className="p-3">
            <Menu>
                <Menu.List title={props.title}>
                    {items}
                </Menu.List>
            </Menu>
        </Section>
    )
}

export default DiscussionList