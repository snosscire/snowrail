import { useEffect, useState } from 'react';
import 'bulma/css/bulma.min.css';
import { Columns } from 'react-bulma-components';
import DiscussionList from './DiscussionList';
import Discussion from './Discussion'
import { EventsOff, EventsOn } from '../wailsjs/runtime/runtime'
import {
    GetUsersForChannel,
    SendMessageToChannel,
    SendMessageToUser
} from "../wailsjs/go/main/App";
import { main } from '../wailsjs/go/models';

type ServerProps = {
    serverName: string
}

let _channels = new Array<string>
let _chats = new Array<string>
let _messages = new Map<string, Array<main.Message>>
let _channelUsers = new Map<string, Array<string>>
let _selectedDiscussion = ''

function Server(props: ServerProps) {

    const [channels, setChannels] = useState(new Array<string>)
    const [chats, setChats] = useState(new Array<string>)
    const [selectedDiscussion, setSelectedDiscussion] = useState('')
    const [messages, setMessages] = useState(new Map<string, Array<main.Message>>)
    const [channelUsers, setChannelUsers] = useState(new Map<string, Array<string>>)

    function handleChannelChange(channelName: string) {
        _selectedDiscussion = channelName
        setSelectedDiscussion(channelName)
        GetUsersForChannel(props.serverName, channelName).then(function(users: Array<string>) {
            updateChannelUsers(channelName, users)
        })
    }

    function updateChannelUsers(channelName: string, users: Array<string>) {
        if (!_channelUsers.has(channelName)) {
            _channelUsers.set(channelName, new Array<string>)
        }
        users.forEach(function(user) {
            if (!_channelUsers.get(channelName)?.includes(user)) {
                _channelUsers.get(channelName)?.push(user)
            }
        })
        setChannelUsers(
            new Map<string, Array<string>>(_channelUsers)
        )
    }

    function handleChatChange(userName: string) {
        _selectedDiscussion = userName
        setSelectedDiscussion(userName)
    }

    function handleChannelJoined(channelName: string) {
        _channels.push(channelName)
        setChannels([
            ..._channels
        ])
        if (_selectedDiscussion.length == 0) {
            _selectedDiscussion = channelName
            setSelectedDiscussion(channelName)
        }
    }

    function handleChannelMessage(message: main.Message) {
        if (!_messages.has(message.source)) {
            _messages.set(message.source, new Array<main.Message>)
        }
        _messages.get(message.source)?.push(message)

        setMessages(
            new Map<string, Array<main.Message>>(_messages)
        )
    }

    function handleUserMessage(message: main.Message) {
        if (!_messages.has(message.source)) {
            _messages.set(message.source, new Array<main.Message>)
        }
        _messages.get(message.source)?.push(message)

        setMessages(
            new Map<string, Array<main.Message>>(_messages)
        )

        if (!_chats.includes(message.source)) {
            _chats.push(message.source)
        }
        setChats([
            ..._chats
        ])
    }

    function handleSendMessageToChannel(message: string) {
        SendMessageToChannel(props.serverName, _selectedDiscussion, message)
    }

    function handleSendMessageToUser(message: string) {
        SendMessageToUser(props.serverName, _selectedDiscussion, message)
    }

    useEffect(function() {
        EventsOn('channel.joined.' + props.serverName, handleChannelJoined)
        EventsOn('channel.message.' + props.serverName, handleChannelMessage)
        EventsOn('user.message.' + props.serverName, handleUserMessage)
    
        return function() {
            EventsOff('channel.joined.' + props.serverName)
            EventsOff('channel.message.' + props.serverName)
            EventsOff('user.message.' + props.serverName)
        }
    }, [])

    function renderChannel(channelName: string, active: boolean) {
        return (
            <Discussion
                key={props.serverName + "." + channelName}
                serverName={props.serverName}
                channelName={channelName}
                messages={messages.get(channelName) || new Array<main.Message>}
                users={channelUsers.get(channelName) || new Array<string>}
                active={active}
                onMessageSend={handleSendMessageToChannel}
            />
        )
    }
    function renderChat(userName: string, active: boolean) {
        return (
            <Discussion
                key={props.serverName + "." + userName}
                serverName={props.serverName}
                channelName={userName}
                messages={messages.get(userName) || new Array<main.Message>}
                active={active}
                onMessageSend={handleSendMessageToUser}
            />
        )
    }

    const renderedChannels = channels.map((channelName: string) => {
        const active = channelName == selectedDiscussion
        return renderChannel(channelName, active)
    })
    const renderedChats = chats.map((userName: string) => {
        const active = userName == selectedDiscussion
        return renderChat(userName, active)
    })

    return (
        <Columns multiline={false} style={{minHeight: '100vh'}}>
            <Columns.Column size={2} backgroundColor="white-bis">
                <DiscussionList
                    title="Channels"
                    selectedChannel={selectedDiscussion}
                    channels={channels}
                    onChange={handleChannelChange}
                />
                <DiscussionList
                    title="Direct Messages"
                    selectedChannel={selectedDiscussion}
                    channels={chats}
                    onChange={handleChatChange}
                />
            </Columns.Column>
            <Columns.Column>
                {renderedChannels}
                {renderedChats}
            </Columns.Column>
        </Columns>
    )
}

export default Server