package main

import (
	"context"
	"fmt"
)

// App struct
type App struct {
	ctx        context.Context
	serverList *ServerList
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx

	server := NewServer(
		ctx,
		"localhost",
		"localhost",
		6667,
		"snowrail",
	)
	a.serverList = NewServerList()
	a.serverList.Add(server)
	server.Connect()

	fmt.Print("Hello World!\n")
}

func (a *App) ConnectToServers() {
	a.serverList.Connect()
}

func (a *App) GetServerNames() []string {
	return a.serverList.GetServerNames()
}

func (a *App) GetChannelsForServer(serverName string) []string {
	server := a.serverList.GetByName(serverName)
	return server.GetChannelNames()
}

func (a *App) GetUsersForChannel(serverName string, channelName string) []string {
	server := a.serverList.GetByName(serverName)
	if server == nil {
		return []string{}
	}
	channel := server.GetChannelByName(channelName)
	if channel == nil {
		return []string{}
	}
	return channel.GetUsers()
}

func (a *App) GetMessagesForChannel(serverName string, channelName string) []*Message {
	server := a.serverList.GetByName(serverName)
	if server == nil {
		return []*Message{}
	}
	channel := server.GetChannelByName(channelName)
	if channel == nil {
		return []*Message{}
	}
	return channel.GetMessages()
}

func (a *App) SendMessageToChannel(serverName string, channelName string, messageText string) {
	server := a.serverList.GetByName(serverName)
	if server == nil {
		return
	}

	channel := server.GetChannelByName(channelName)
	if channel == nil {
		return
	}

	server.SendMessage(channel.name, messageText)
}

func (a *App) SendMessageToUser(serverName string, userName string, messageText string) {
	server := a.serverList.GetByName(serverName)
	if server == nil {
		return
	}

	server.SendMessage(userName, messageText)
}
