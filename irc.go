package main

import (
	"context"
	"fmt"
	"net"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/wailsapp/wails/v2/pkg/runtime"
	"gopkg.in/irc.v4"
)

type ServerStatus int

const (
	Disconnected ServerStatus = iota
	Connecting
	Connected
)

type Server struct {
	ctx      context.Context
	mutex    sync.RWMutex
	status   ServerStatus
	name     string
	host     string
	port     uint
	channels []*Channel
	users    []*User
	client   *irc.Client
	config   irc.ClientConfig
}

type Channel struct {
	name     string
	users    []*User
	messages []*Message
}

type User struct {
	nick     string
	operator bool
	messages []*Message
}

type Message struct {
	Sender string `json:"sender"`
	Source string `json:"source"`
	Time   string `json:"time"`
	Text   string `json:"text"`
	Key    string `json:"key"`
}

type ServerList struct {
	servers []*Server
	mutex   sync.RWMutex
}

func NewServerList() *ServerList {
	l := new(ServerList)
	l.servers = make([]*Server, 0)
	return l
}

func (l *ServerList) Add(server *Server) {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	l.servers = append(l.servers, server)
}

func (l *ServerList) GetServerNames() []string {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	names := make([]string, 0)
	for _, s := range l.servers {
		names = append(names, s.name)
	}
	return names
}

func (l *ServerList) GetByName(name string) *Server {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	for _, s := range l.servers {
		if s.name == name {
			return s
		}
	}
	return nil
}

func (l *ServerList) Connect() {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	for _, s := range l.servers {
		if s.status == Disconnected {
			s.Connect()
		}
	}
}

func NewServer(ctx context.Context, name string, host string, port uint, nick string) *Server {
	s := new(Server)
	s.ctx = ctx
	s.name = name
	s.host = host
	s.port = port
	s.channels = make([]*Channel, 0)
	s.config.Name = nick
	s.config.Nick = nick
	s.config.User = nick
	s.config.Handler = s
	return s
}

func (s *Server) Connect() error {
	s.status = Connecting
	runtime.EventsEmit(s.ctx, "server.connecting", s.name)
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", s.host, s.port))
	if err != nil {
		return err
	}

	s.client = irc.NewClient(conn, s.config)
	go s.client.RunContext(s.ctx)
	return nil
}

func (s *Server) GetChannelNames() []string {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	names := make([]string, 0)
	for _, c := range s.channels {
		names = append(names, c.name)
	}
	return names
}

func (s *Server) GetChannelByName(name string) *Channel {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	for _, c := range s.channels {
		if c.name == name {
			return c
		}
	}
	return nil
}

func (s *Server) GetOrCreateUser(nick string) *User {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	var user *User
	for _, u := range s.users {
		if u.nick == nick {
			user = u
			break
		}
	}
	if user == nil {
		user = NewUser(nick)
		s.users = append(s.users, user)
	}
	return user
}

func (s *Server) SendMessage(to string, text string) {
	if strings.HasPrefix(to, "#") || strings.HasPrefix(to, "&") {
		channel := s.GetChannelByName(to)
		if channel != nil {
			s.client.Writef("%s %s %s", "PRIVMSG", channel.name, text)
			message := NewMessage(s.client.CurrentNick(), channel.name, text)
			channel.messages = append(channel.messages, message)
			runtime.EventsEmit(s.ctx, "channel.message."+s.name, message)
		}
	} else {
		user := s.GetOrCreateUser(to)
		s.client.Writef("%s %s %s", "PRIVMSG", user.nick, text)
		message := NewMessage(s.client.CurrentNick(), user.nick, text)
		user.messages = append(user.messages, message)
		runtime.EventsEmit(s.ctx, "user.message."+s.name, message)
	}
}

func (s *Server) Handle(client *irc.Client, message *irc.Message) {
	fmt.Println(message.String())
	switch message.Command {
	case "001":
		s.handleConnected()
	case "353":
		s.handleNamesList(message)
	case "PRIVMSG":
		s.handlePrivMsg(message)
	default:
		//fmt.Printf("Command: %s, Message: %s\n", message.Command, message.String())
	}
}

func (s *Server) handleConnected() {
	s.status = Connected
	runtime.EventsEmit(s.ctx, "server.connected", s.name)
	s.joinChannels()
}

func (s *Server) handleNamesList(message *irc.Message) {
	if len(message.Params) == 4 {
		channelName := message.Params[2]
		nicks := strings.Split(message.Params[3], " ")
		channel := s.GetChannelByName(channelName)
		if channel != nil {
			for _, nick := range nicks {
				channel.AddUser(nick)
			}
		}
	}
}

func (s *Server) handlePrivMsg(ircMessage *irc.Message) {
	to := ircMessage.Params[0]
	text := ircMessage.Params[1]
	if strings.HasPrefix(to, "#") || strings.HasPrefix(to, "&") {
		channel := s.GetChannelByName(to)
		if channel != nil {
			message := NewMessage(ircMessage.Prefix.Name, channel.name, text)
			channel.messages = append(channel.messages, message)
			runtime.EventsEmit(s.ctx, "channel.message."+s.name, message)
		}
	} else {
		user := s.GetOrCreateUser(ircMessage.Prefix.Name)
		if user != nil {
			message := NewMessage(ircMessage.Prefix.Name, ircMessage.Prefix.Name, text)
			user.messages = append(user.messages, message)
			runtime.EventsEmit(s.ctx, "user.message."+s.name, message)
		}
	}
}

func (s *Server) joinChannels() {
	s.channels = append(s.channels, NewChannel("#foo"))
	s.channels = append(s.channels, NewChannel("#bar"))
	s.channels = append(s.channels, NewChannel("#baz"))
	s.client.Write("JOIN #foo")
	s.client.Write("JOIN #bar")
	s.client.Write("JOIN #baz")
	runtime.EventsEmit(s.ctx, "channel.joined."+s.name, "#foo")
	runtime.EventsEmit(s.ctx, "channel.joined."+s.name, "#bar")
	runtime.EventsEmit(s.ctx, "channel.joined."+s.name, "#baz")
}

func NewChannel(name string) *Channel {
	channel := new(Channel)
	channel.name = name
	return channel
}

func (c *Channel) AddUser(nick string) {
	c.users = append(c.users, NewUser(nick))
}

func (c *Channel) GetUsers() []string {
	nicks := make([]string, 0)
	for _, u := range c.users {
		nicks = append(nicks, u.nick)
	}
	return nicks
}

func (c *Channel) GetMessages() []*Message {
	return c.messages
}

func NewUser(nick string) *User {
	user := new(User)
	if strings.HasPrefix(nick, "@") {
		user.operator = true
		nick = nick[1:]
	}
	user.nick = nick
	return user
}

func NewMessage(sender string, source string, text string) *Message {
	message := new(Message)
	message.Sender = sender
	message.Source = source
	message.Time = time.Now().Format("15:04")
	message.Text = text
	message.Key = uuid.NewString()
	return message
}
